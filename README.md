# Junior Javascript Test #

### DESCRIPTION ###

This is a simple exercise to test your JavaScript, html and CSS skills while building a news search App 
step by step. The app will use the Hacker News API to fetch news and will display the results in a table 
with default 10 results. 

Each result row will include 3 columns with a link to the article/news, author and number of points. See the wireframe below:

![Scheme](images/mockup.png)

When the user clicks on the title of an article listed in the results, the app will redirect to the article’s url.
The files to be completed are:

•	test/index.html		
•	test/assets/js/app.js	 
•	test/assets/css/main.css


The files are partially completed apart from main.css, which will be completed in the last steps. They include tips and references 
to the places where the code should be included. 

For Javascript:

~~~~
/*
STEP 1: Add code here
*/
~~~~

And for html:

~~~~
<!--
STEP 7: Add code here
-->
~~~~

The exercise must be completed in vanilla JavaScript. 


### EXERCISE ###

**STEP 1:** Add ‘worldcup 2018’ as the default search term when the page loads.

**STEP 2:** Execute default ‘worldcup 2018’ search on page load.

**STEP 3:** Add an event handler to the search form. The function searchHandler(event) included in app.js must be invoked when the event is triggered. 

**STEP 4:** Complete searchHandler(event) function. At the end of this step the app must fetch the API to search news using the search term entered in the input text as the ‘query’ parameter. 

**STEP 5:** Process the search results. This step consists in extracting the results from the response object and invoking the renderResults.

**STEP 6:** Display the search results in a table or div container.

**STEP 7:** Filter results with null/empty title from the search results. Apply the filter before displaying the results.

**STEP 8:** Filter results with null/empty url from the search results. Apply the filter before displaying the results.

**STEP 9:** Add a ‘More’ button at the bottom of the search results table.

**STEP 10:** Add ‘onclick’ event  handler to the ‘More’ button.

**STEP 11:** Append the next 10 search results to the results table for the current search term.

**STEP 12 (Bonus step):** Add CSS to the page to resemble the captures in the appendix. 

### Hawker News API search endpoint ###

See https://hn.algolia.com/api for details

~~~~
GET  http://hn.algolia.com/api/v1/search?query=...
~~~~

The url parameters required for the exercise are:

URL parameter |	Description
--------------|-------------
query	      | Search term. Eg: ‘AWS submit’
page	      | Page number
hitsPerPage	  | Number of results per page. Eg: 10 means 10 results per page.


~~~~
Eg: GET https://hn.algolia.com/api/v1/search?query=worldcup&page=0&hitsPerPage=10  
~~~~
returns The first page of 10 results for the search term ‘worldcup’. The result is a JSON response including the following attributes:



~~~~
{
	"hits":
		[
			{
				"created_at":"2018-06-12T19:36:35.000Z",
				"title":"Prediction of the FIFA World Cup 2018 – A random forest approach",
				"url":"https://arxiv.org/abs/1806.03208", 
				"author":"ajonnav",
				"points":48,						
				"num_comments":57,						
			},
			... 				
		],
	
	"page":0,
	
	"hitsPerPage":10,			
	"query":"worldcup"
}
~~~~

### Screenshots of the app after completing the steps ###

![Scheme](images/initPage.png)

![Scheme](images/initPageLoadMore.png)

![Scheme](images/initPageLoadMore3.png)

