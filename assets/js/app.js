/*
	Default search term
*/
const DEFAULT_QUERY = 'worldcup 2018';
/*
	Default page size
*/
const DEFAULT_HPP = '10';
/*
	Hacker News API base URL
*/
const PATH_BASE = 'https://hn.algolia.com/api/v1';
/*
	Hacker News API seach endpoint path
*/
const PATH_SEARCH = '/search';
/*
	Query url parameter
*/
const PARAM_SEARCH = 'query=';
/*
	Page url parameter
*/
const PARAM_PAGE = 'page=';
/*
	Results per page
*/
const PARAM_HPP = 'hitsPerPage=';

/*
	Current search term
*/
let searchKey = DEFAULT_QUERY;
/*
	Array to cache result objects
*/
let results = [];

/*
	Executed on page load.
*/
function ready(){
	
	/*
		STEP 1: add code here.
		TIP: use document.getElementById().
	*/
	
	/*
		STEP 2: add code here.
		TIP: invoke fetchStories() function to perform the default search.
	*/	

}
/*
	'Search' button event handler
	 STEP 3: This is the event handler
*/
function searchHandler(event){

	/*
	   STEP 4: Add code here.
	   TIP: don't forget to prevent the default behaviour when a form is submitted. Otherwise the
	        page will be reloaded.

	   TIP: fetchStories() function fetches the API.    
	*/    
    
}
/*
	Fetches the news by calling Hawker News API /search endpoint: 
	https://hn.algolia.com/api/v1/search?query=cats&page=0&hitsPerPage=10

	TIP: searchTerm and page arguments have default values of '' and 0.
	     The default values are applied if the function is invoked without parameters. Eg:

	     fetchStories() call will use searchTerm='' and page=0
	     fetchStories('Hello') call will use searchTerm='Hello' and default page = 0 
*/
function fetchStories(searchTerm='', page=0){
    // Fetchs the search endpopint
	fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_PAGE}${page}&${PARAM_HPP}${DEFAULT_HPP}`)
      // Maps the response to a json object including the results
      .then(response => response.json())
      // Calls processResults function to process the results
      .then(result => processResults(result))
      // If an error occurs, then calls handleError function
      .catch(e => handleError(e));

      /*
		When fetching the endpoint with the url: https://hn.algolia.com/api/v1/search?query=worldcup%202018&page=0&hitsPerPage=10

		response.json() will transform the JSON response to a javascript object including the following attributes:

		{
			"hits":
			    [   
					{   
						"created_at":"2018-06-12T19:36:35.000Z",
						"title":"Prediction of the FIFA World Cup 2018 – A random forest approach",
						"url":"https://arxiv.org/abs/1806.03208", 
						"author":"ajonnav",
						"points":48,
						"story_text":null,
						"comment_text":null,
						"num_comments":57,
						"story_id":null,
						"story_title":null,
						"story_url":null,
						"parent_id":null,
						"created_at_i":1528832195,
						"_tags":["story","author_ajonnav","story_17297178"],
						"objectID":"17297178",
						"_highlightResult":{"title":{"value":"Prediction of the FIFA \u003cem\u003eWorld Cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e – A random forest approach","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://arxiv.org/abs/1806.03208","matchLevel":"none","matchedWords":[]},"author":{"value":"ajonnav","matchLevel":"none","matchedWords":[]}}
					},
					{"created_at":"2018-06-14T11:12:19.000Z","title":"Show HN: World Cup 2018 Predictions with Bayesian ML","url":"https://kickoff.ai","author":"lum","points":26,"story_text":null,"comment_text":null,"num_comments":8,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1528974739,"_tags":["story","author_lum","story_17310769","show_hn"],"objectID":"17310769","_highlightResult":{"title":{"value":"Show HN: \u003cem\u003eWorld Cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e Predictions with Bayesian ML","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://kickoff.ai","matchLevel":"none","matchedWords":[]},"author":{"value":"lum","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-21T09:43:32.000Z","title":"Show HN: A world cup 2018 live dashboard with browser notifications","url":"https://victorparmar.github.io/worldcup-live/","author":"wheresvic1","points":8,"story_text":null,"comment_text":null,"num_comments":1,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1529574212,"_tags":["story","author_wheresvic1","story_17363496","show_hn"],"objectID":"17363496","_highlightResult":{"title":{"value":"Show HN: A \u003cem\u003eworld cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e live dashboard with browser notifications","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://victorparmar.github.io/\u003cem\u003eworldcup\u003c/em\u003e-live/","matchLevel":"partial","fullyHighlighted":false,"matchedWords":["worldcup"]},"author":{"value":"wheresvic1","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-17T23:47:38.000Z","title":"Show HN: A world cup 2018 live dashboard with browser notifications","url":"https://victorparmar.github.io/worldcup-live/","author":"wheresvic1","points":7,"story_text":null,"comment_text":null,"num_comments":5,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1529279258,"_tags":["story","author_wheresvic1","story_17335026","show_hn"],"objectID":"17335026","_highlightResult":{"title":{"value":"Show HN: A \u003cem\u003eworld cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e live dashboard with browser notifications","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://victorparmar.github.io/\u003cem\u003eworldcup\u003c/em\u003e-live/","matchLevel":"partial","fullyHighlighted":false,"matchedWords":["worldcup"]},"author":{"value":"wheresvic1","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-04-14T16:45:22.000Z","title":"Show HN: FIFA World Cup 2018 prediction game live on Ethereum Ropsten","url":"https://cryptocup.io","author":"lucasverra","points":7,"story_text":null,"comment_text":null,"num_comments":5,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1523724322,"_tags":["story","author_lucasverra","story_16838115","show_hn"],"objectID":"16838115","_highlightResult":{"title":{"value":"Show HN: FIFA \u003cem\u003eWorld Cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e prediction game live on Ethereum Ropsten","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://cryptocup.io","matchLevel":"none","matchedWords":[]},"author":{"value":"lucasverra","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-24T16:23:49.000Z","title":"Show HN: A World Cup 2018 CLI dashboard – Watch matches in your terminal","url":"https://github.com/cedricblondeau/world-cup-2018-cli-dashboard","author":"cedricblondeau","points":5,"story_text":null,"comment_text":null,"num_comments":1,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1529857429,"_tags":["story","author_cedricblondeau","story_17387102","show_hn"],"objectID":"17387102","_highlightResult":{"title":{"value":"Show HN: A \u003cem\u003eWorld Cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e CLI dashboard – Watch matches in your terminal","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://github.com/cedricblondeau/\u003cem\u003eworld-cup\u003c/em\u003e-\u003cem\u003e2018\u003c/em\u003e-cli-dashboard","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"author":{"value":"cedricblondeau","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-03T14:41:53.000Z","title":"Show HN: Ethercup: Place Ether on your fav teams of the FIFA Worldcup 2018","url":null,"author":"mohoff","points":5,"story_text":"Hi guys,\u003cp\u003eI am building Ethercup and I wanted to show you how it works: https:\u0026#x2F;\u0026#x2F;ethercup.github.io\u0026#x2F; .\u003cp\u003eOn the website is a demo bet live right now on the Kovan test network. This bet opens today and will close in a few days. A How-to is provided on the website, but you can check out the code directly on GitHub: https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;ethercup\u0026#x2F;ethercup.github.io\u003cp\u003eThere\u0026#x27;s no bug-bounty program, but I\u0026#x27;m willing to share profits (if it\u0026#x27;ll yield any) in case you can break it and point me to it. Looking forward to hearing your feedback :) Also, feel free to ask questions.\u003cp\u003eUPDATE: Betting is open now! Place your bets!","comment_text":null,"num_comments":0,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1528036913,"_tags":["story","author_mohoff","story_17219741","show_hn"],"objectID":"17219741","_highlightResult":{"title":{"value":"Show HN: Ethercup: Place Ether on your fav teams of the FIFA \u003cem\u003eWorldcup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"author":{"value":"mohoff","matchLevel":"none","matchedWords":[]},"story_text":{"value":"Hi guys,\u003cp\u003eI am building Ethercup and I wanted to show you how it works: https://ethercup.github.io/ .\u003cp\u003eOn the website is a demo bet live right now on the Kovan test network. This bet opens today and will close in a few days. A How-to is provided on the website, but you can check out the code directly on GitHub: https://github.com/ethercup/ethercup.github.io\u003cp\u003eThere's no bug-bounty program, but I'm willing to share profits (if it'll yield any) in case you can break it and point me to it. Looking forward to hearing your feedback :) Also, feel free to ask questions.\u003cp\u003eUPDATE: Betting is open now! Place your bets!","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-18T20:36:42.000Z","title":"Show HN: A world cup 2018 live dashboard with browser notifications","url":"https://victorparmar.github.io/worldcup-live/","author":"wheresvic1","points":4,"story_text":null,"comment_text":null,"num_comments":0,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1529354202,"_tags":["story","author_wheresvic1","story_17341499","show_hn"],"objectID":"17341499","_highlightResult":{"title":{"value":"Show HN: A \u003cem\u003eworld cup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e live dashboard with browser notifications","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://victorparmar.github.io/\u003cem\u003eworldcup\u003c/em\u003e-live/","matchLevel":"partial","fullyHighlighted":false,"matchedWords":["worldcup"]},"author":{"value":"wheresvic1","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-12T00:49:40.000Z","title":"The finest Worldcup 2018 CLI, a software engineer prepares for his Worldcup","url":"https://github.com/codeaholicguy/wowcup","author":"codeaholicguy","points":3,"story_text":null,"comment_text":null,"num_comments":1,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1528764580,"_tags":["story","author_codeaholicguy","story_17289903"],"objectID":"17289903","_highlightResult":{"title":{"value":"The finest \u003cem\u003eWorldcup\u003c/em\u003e \u003cem\u003e2018\u003c/em\u003e CLI, a software engineer prepares for his \u003cem\u003eWorldcup\u003c/em\u003e","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://github.com/codeaholicguy/wowcup","matchLevel":"none","matchedWords":[]},"author":{"value":"codeaholicguy","matchLevel":"none","matchedWords":[]}}},{"created_at":"2018-06-25T16:00:39.000Z","title":"How We Built the 2018 World Cup GraphQL API","url":"https://medium.freecodecamp.org/building-the-2018-world-cup-graphql-api-fab40ccecb9e","author":"jexp","points":3,"story_text":null,"comment_text":null,"num_comments":0,"story_id":null,"story_title":null,"story_url":null,"parent_id":null,"created_at_i":1529942439,"_tags":["story","author_jexp","story_17393420"],"objectID":"17393420","_highlightResult":{"title":{"value":"How We Built the \u003cem\u003e2018\u003c/em\u003e \u003cem\u003eWorld Cup\u003c/em\u003e GraphQL API","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"url":{"value":"https://medium.freecodecamp.org/building-the-\u003cem\u003e2018\u003c/em\u003e-\u003cem\u003eworld-cup\u003c/em\u003e-graphql-api-fab40ccecb9e","matchLevel":"full","fullyHighlighted":false,"matchedWords":["worldcup","2018"]},"author":{"value":"jexp","matchLevel":"none","matchedWords":[]}}}
				],
			"nbHits":245,
			"page":0,
			"nbPages":25,
			"hitsPerPage":10,
			"processingTimeMS":8,
			"exhaustiveNbHits":true,
			"query":"worldcup 2018",
			"params":"advancedSyntax=true\u0026analytics=false\u0026hitsPerPage=10\u0026page=0\u0026query=worldcup+2018"
		}     
		
      */

      /*
		TIP: Only "title", "url", "author" and "points" attributes are requiered. Ignore the rest of attributes
      */

}

/*
	Processes the search result object
*/
function processResults(result){   

	/*

		STEP 5: Add code here.

		TIP: use console.log(result) to see the object's structure. 
		     The search results are included in the object's hits array attribute. Use console.log(result.hits) to see the structure.

	*/    	
    
    renderResults(); // STEP 6: Change this line to include an array of search result items in the call.  
    
}

function renderResults(searchResults = []){	

	/*
		STEP 6: Add code here.

		TIP: Loop through searchResults array and generate html to include them in a table or div
		     depending on your preferences. Generate html and inject it in 
		     <div id="searchResults" class="table"> (Remember innerHTML property!)

        TIP: Functional code is very poweful: map-reduce could be used instead of a traditional loop.  
        	 Template literals could be used to generate the html.

	*/

	/*
		STEP 7 and 8: Add code here.

		TIP: Filter the results with either empty/null url or title. Loop through searchResults array.
			 Complete the isEmpty() included in this file.	
			 Filter could be added to a filter-map-reduce pipeline if funtional code was used in STEP 6.


	*/

}

function isEmpty(value){

	/*
		STEP 7 and 8: Add code here.
	*/
    
}

/*
	'More' button event handler
*/
function moreResultsHandler(event){
	/*
		STEP 11: Add code here.

		TIP: Search results and current page per search term must be cached so that
		     the event handler knows which is the next page to load.  
			 
			 Use fetchStories function and pass the next page to load.
	*/
	
}

function handleError(e){
	    
    document.getElementById('searchResults').innerHTML = '<p>Something went wrong.</p>';
        
}